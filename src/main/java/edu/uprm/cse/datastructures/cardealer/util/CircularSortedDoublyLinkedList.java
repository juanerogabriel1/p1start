package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	
	/**
	 * Implements Node
	 */
	private static class Node<E>{
		private E element;
		private Node<E> next;
		private Node<E> prev;
		
		public Node(){
			this.element = null;
			this.next = null;
			this.prev = null;
		}
		
		public Node(E e){
			element = e;
			this.next = null;
			this.prev = null;
		}
		
		public Node(E e, Node<E> p , Node<E> n){
			this.element = e;
			prev = p;
			next = n;
		}
		
		public E getElement(){
			return element;
		}
		public void setElement(E element){
			this.element = element;
		}
		public Node<E> getNext(){
			return next;
		}
		public void setNext(Node<E> next){
			this.next = next;
		}
		public Node<E> getPrev(){
			return prev;
		}
		public void setPrev(Node<E> prev){
			this.prev = prev;
		}
		public void clear(){
			prev = null;
			next = null;
		}
	}
	/**
	 * Implements Iterator
	 */
	private class CircularLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;

		public CircularLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}

	}
	
	private int currentSize;
	private Node<E> header;
	private Comparator<E> cmp;
	
	public CircularSortedDoublyLinkedList(){
		this.currentSize = 0;
		this.header = new Node<>();
		
		this.header.setNext(header);
		this.header.setPrev(header);
		
		
	}

	
	public CircularSortedDoublyLinkedList(Comparator<E> c){
		this();
		this.cmp = c;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new CircularLinkedListIterator<E>();
	}
	/**
	* @param e - element that will be added into the list
	*/
	@Override
	public boolean add(E e) {
		Node<E> temp = new Node<E>(e);
		
		if(e == null){
			return false;
		}
		if(this.isEmpty()){
			this.header.setNext(temp);
			this.header.setPrev(temp);
			temp.setNext(header);
			temp.setPrev(header);
			this.currentSize++;
			return true;
		}
		Node<E> currentNode = this.header.getNext();
		
		while(currentNode != this.header){
			if(cmp.compare( e,  currentNode.getElement()) <= 0){
				temp.setPrev(currentNode.getPrev());
				temp.setNext(currentNode);
				currentNode.setPrev(temp);
				temp.getPrev().setNext(temp);
				this.currentSize++;
				return true;
			}
			currentNode = currentNode.getNext();
		}
		temp.setNext(header);
		temp.setPrev(header.getPrev());
		header.getPrev().setNext(temp);
		header.setPrev(temp);
		this.currentSize++;
		
		return true;
	}
	/**
	 * returns the size of the list
	 */
	@Override
	public int size() {
		return this.currentSize;
	}
	
	/**
	 * @param e - element that will be removed from the list
	 */
	@Override
	public boolean remove(E e) {
		Node<E> temp;
		for(temp = header.getNext(); temp != header; temp = temp.getNext()){
			if(temp.getElement().equals(e)){
				temp.getNext().setPrev(temp.getPrev());
				temp.getPrev().setNext(temp.getNext());
				temp.clear();
				this.currentSize--;
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param index - remove an object from the list with position index
	 */
	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException{
		if(index<0 || index >= this.currentSize || this.isEmpty()) {
			throw new IndexOutOfBoundsException();
			}

		int position = 0;
		
		Node<E> current=header.getNext();
		
		while(position < index) {
			current=current.getNext();
			position++;
		}
		current.getNext().setPrev(current.getPrev());
		current.getPrev().setNext(current.getNext());
		current.clear();
		this.currentSize--;

		return true;
	}
	/**
	* returns the list of cars as an array
	*/
	public Car[] toArray(){
		Car [] result = new Car[this.currentSize];
		int i = 0;
		Node<E> temp = this.header.getNext();
		while(temp != this.header) {
			result[i] = (Car) temp.getElement();
			i++;
			temp = temp.getNext();
		}
		return result;
	}
	
	/**
	 * @param e - removes all elements e from the list
	 */
	@Override
	public int removeAll(E e) {
		int count=0;
		while(this.remove(e)) {
			count++;
		}
		return count;
	}
	
	/**
	 * return the first element in the list
	 */
	@Override
	public E first() {
		if(isEmpty()) {
			return null;
			}
		return header.getNext().getElement();
	}
	
	/**
	 * returns the last element in the list
	 */
	@Override
	public E last() {
		if(isEmpty()) {
			return null;
			}

		return header.getPrev().getElement();
	}
	
	/**
	 * @param index - returns the value of the element in the index given
	 */
	@Override
	public E get(int index) {
		if(isEmpty()) {
			return null;
			}

		if(index < 0 || index >= this.currentSize) {
			return null;
			}

		else {
			int pos=0;
			Node<E>current=header.getNext();
			while(pos < index) {
				current=current.getNext();
				pos++;
			}
			return current.getElement();
		}
	}
	
	/**
	 * Will empty the entire list
	 */
	@Override
	public void clear() {
		while(!isEmpty()){
			this.remove(0);
		}
	}
	
	/**
	 * @param e - element that will be searched for in the list
	 */
	@Override
	public boolean contains(E e) {
		if(isEmpty()|| e==null) {
			return false;
			}

		Node<E> current;
		for(current=header.getNext(); current!=header; current=current.getNext()) {
			if(current.getElement().equals(e)) 
				return true;
		}
		return false;
	}
	
	/**
	 * will return true or false depending if the list is empty
	 */
	@Override
	public boolean isEmpty() {
		return this.currentSize == 0;
	}
	
	/**
	 * @param e - will return the first index in which the element e is in
	 */
	@Override
	public int firstIndex(E e) {
		int pos=0;
		Node<E>temp;			
		for(temp=header.getNext(); temp!=header; temp=temp.getNext(), pos++) {
			if(e.equals(temp.getElement()))
				return pos;
		}
		return -1;
	}
	
	/**
	 * @param e - will return the last index in which the element e is in
	 */
	@Override
	public int lastIndex(E e) {
		int pos=size()-1;
		Node<E>temp;
		for(temp=header.getPrev(); temp!=header; temp=temp.getPrev(), pos--) {
			if(e.equals(temp.getElement()))
				return pos;
		}
		return -1; 
	}

}
